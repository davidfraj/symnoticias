<?php
//src/AppBundle/Controller/LoginController.php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;

use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;

class LoginController extends Controller
{

	private $session;
	
	public function __construct() {
		$this->session=new Session();
	}


    		 /**
		     * @Route("/login", name="homepage_login")
		     */
		    public function loginAction(Request $request)
		    {
		        // $authenticationUtils=$this->get("security.authentication_utils");
		        // $error=$authenticationUtils->getLastAuthenticationError();
		        // $lastUsername=$authenticationUtils->getLastUsername();
		        // return $this->render("login/login.html.twig", array("error"=>$error, "last_username"=>$lastUsername));
		        return $this->render("login/login.html.twig");
		    }

		    /**
		     * @Route("/login_check", name="login_check")
		     */
		    public function login_checkAction(Request $request)
		    {

		    }

		    /**
		     * @Route("/logout", name="logout")
		     */
		    public function logoutAction(Request $request)
		    {

		    }

		    /**
		     * @Route("/registro", name="login_registro")
		     */
		    public function registroAction(Request $request)
		    {
		    	//Me tengo que crear un formulario para registrarme

		    		$usuario = new Usuario();
					$form = $this->createForm(UsuarioType::class,$usuario);
					

					$form->handleRequest($request);

					if($form->isSubmitted()){

						//$usuario->setName($form->get("nombre")->getData());
						//$usuario->setSurname($form->get("apellidos")->getData());
						//$usuario->setEmail($form->get("email")->getData());

						$factory = $this->get("security.encoder_factory");
						$encoder = $factory->getEncoder($usuario);
						$password = $encoder->encodePassword($form->get("password")->getData(), $usuario->getSalt());

						$usuario->setPassword($password);
						$usuario->setRole("ROLE_USER");
					

						$em = $this->getDoctrine()->getEntityManager();
						$em->persist($usuario);
						$flush = $em->flush();

						if($flush==null){
							// return $this->redirectToRoute('homepage');
							$mensaje="Insertado con exito";
							$clase="alert-success";
						}else{
							$mensaje="Error al insertar";
							$clase="alert-error";
						}

						$this->session->getFlashBag()->add("mensaje",$mensaje);
						$this->session->getFlashBag()->add("clase",$clase);



					}

					return $this->render("login/registro.html.twig", array(
						"form" => $form->createView()
					));



		    		//TODO: Por TERMINAR
		    		//TODO: Por TERMINAR

					// $form->handleRequest($request);
					// if($form->isSubmitted()){
					// 	if($form->isValid()){
					// 		$em=$this->getDoctrine()->getEntityManager();
					// 		$user_repo=$em->getRepository("BlogBundle:User");
					// 		$user = $user_repo->findOneBy(array("email"=>$form->get("email")->getData()));
							
					// 		if(count($user)==0){
					// 			$user = new User();
					// 			$user->setName($form->get("name")->getData());
					// 			$user->setSurname($form->get("surname")->getData());
					// 			$user->setEmail($form->get("email")->getData());

					// 			$factory = $this->get("security.encoder_factory");
					// 			$encoder = $factory->getEncoder($user);
					// 			$password = $encoder->encodePassword($form->get("password")->getData(), $user->getSalt());

					// 			$user->setPassword($password);
					// 			$user->setRole("ROLE_USER");
					// 			$user->setImagen(null);

					// 			$em = $this->getDoctrine()->getEntityManager();
					// 			$em->persist($user);
					// 			$flush = $em->flush();
					// 			if($flush==null){
					// 				$status = "El usuario se ha creado correctamente";
					// 			}else{
					// 				$status = "No te has registrado correctamente";
					// 			}
					// 		}else{
					// 			$status = "El usuario ya existe!!!";
					// 		}
					// 	}else{
					// 		$status = "No te has registrado correctamente";
					// 	}

					// 	$this->session->getFlashBag()->add("status",$status);
					// }

					// return $this->render("login/login.html.twig", array(
					// 	"error" => $error,
					// 	"last_username" => $lastUsername,
					// 	"form" => $form->createView()
					// ));

					//TODO: Por TERMINAR
		    		//TODO: Por TERMINAR
					
					 



		    }

}
